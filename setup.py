from setuptools import setup, find_packages

import wavefier_wdf

setup(
    name='wavefier_wdf',
    author=wavefier_wdf.__author__,
    version=wavefier_wdf.__version__,
    description='Wavefier WDF - WDF pipeline library',
    url='',
    author_email='filipmor92@gmail.com',
    packages=find_packages(),
    package_data = {
        'wavefier_wdf.preprocessing': ['*.txt']
    },
    install_requires=[ 
        'wavefier-common==2.0.0', 
        'wavefier-trigger-handlers==2.0.0',
        'wavefier_import_handlers==2.0.0'
    ],
    dependency_links=[
      "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-common.git#egg=wavefier-common-2.0.0",
      "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-trigger-handlers.git#egg=wavefier-trigger-handlers-2.0.0",
      "git+https://gitlab+deploy-token-661042:fb4XV93yiAjJXsT3K3f2@gitlab.com/wavefier/wavefier-import-handlers.git#egg=wavefier_import_handlers-2.0.0"
   ]
)

