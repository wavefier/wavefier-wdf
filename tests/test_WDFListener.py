import os
from unittest import TestCase

from wavefier_common.RawData import RawData
from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.util.Path import Path

from wavefier_import_handlers.RawSupplier import RawSupplier

from wavefier_wdf.WDFListener import WDFListener

broker = "kafka_test:9092"
os.environ['KAFKA_BROKER'] = broker


class TestUtil():
    @staticmethod
    def sendMessage():
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_data')
        file_path = str(Path(subdir)/'V-ReplayOPA2-1222713350.gwf')

        f = open(file_path, "rb")
        message = RawData("V-ReplayOPA2-1222713350.gwf", f.read())

        producer = KafkaProducer({'bootstrap.servers': broker})
        producer.publish_message(message)


class TestWDFListener(TestCase):

    def test_listener(self):

        # Send the data to kafka cluster
        TestUtil.sendMessage()

        # Run the listener, consume and process the data
        sup = RawSupplier(broker)
        sup.addRawListener(WDFListener())
        sup.startObserving()
        sup.stopObserving()

        sup.join()
