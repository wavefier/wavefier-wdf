import logging
import os

import datetime as dt
from time import time

from wavefier_import_handlers.RawListener import RawListener
from wavefier_import_handlers.RawSupplier import RawSupplier

from wavefier_common.RawData import RawData

from wavefier_wdf.util.SVCreator import SVCreator
from wavefier_wdf.util.StaticSVCreator import StaticSVCreator
from wavefier_wdf.preprocessing.ApplyStaticWhitening import ApplyStaticWhitening
from wdf.processes.whitening import Whitening
#   from wdf.config.withening.WitheningDataCoef import WitheningDataCoef               # ALB: removed, not present in wdf master branch  anymore

from wdf.config.WdfParameters import WdfParameters
from wdf.config.WhtParameters import WhtParameters
from wdf.config.DataParameters import DataParameters
from wdf.config.DownsampParameters import DownsampParameters

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class WhiteningCoefficientsDiscoverer(RawListener):

    def __init__(self,whiten: Whitening, learnStepInSec: int, wdf_parameters: WdfParameters, wht_params: WhtParameters, downsamp_params: DownsampParameters, data_parameters: DataParameters):
        # Initialize parameters
        self.static_sv = StaticSVCreator(wdf_parameters, wht_params, downsamp_params, data_parameters)
        self.lastEstimation = 0 
        self.learnStepInSec=learnStepInSec                 # WatcherRawData calls WhiteningCoefficientsDiscoverer(whiten, wht_params.learn_step)
        self.whiten= whiten
        self.data_parameters = data_parameters
        self.len = data_parameters.len

    def estimateWhitening(self, raw_data: RawData):
        logging.info("Static Whitening preparation")
        static_flag = self.static_sv.generate(raw_data)   # generate() calls initial_sv() 
        self.sv_size = self.len #.static_sv.single_data_size         # single_data_size internally defined in call method initial_sv()  

        if static_flag:
            # Run Adaptive whitening
            self.whiten.ParametersEstimate(self.static_sv.static_data)
            self.lastEstimation = 0    # everytime whitening coefficients are estimated, restart counter 
        else:
            logging.warning("Static Flag False")


    def time_since_estimate(self, msg_consumed):
        if msg_consumed.getData() is not None:
            self.lastEstimation += self.len  # add to counter every message consumed ----> NOT SURE ABOUT THIS!


    def update(self, context, msg_consumed):
        if msg_consumed is not None:
            if msg_consumed.getData() is not None:
                ## TODO: REVIEW SUL QUANDO SCATTA IL LEARNING
                if ((self.whiten.GetSigma()<1e-50) or (self.lastEstimation > self.learnStepInSec)):
                    self.estimateWhitening(msg_consumed)
                else:
                    self.time_since_estimate(msg_consumed)  # add to counter every message consumed ----> NOT SURE ABOUT THIS!
                    logging.info("Learning is not required...")



