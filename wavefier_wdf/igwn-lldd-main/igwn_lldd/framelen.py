#!/usr/bin/env python

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022) and
# Laser Interferometer Gravitational-Wave Observatory (LIGO) (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#              Brockill <brockill@uwm.edu>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

# These routines are the Python translation of Kipp Cannon's algorithms
# in GstLAL. In particular, they come from framecpp_igwdparse.cc, which
# in August of 2018 can be found here:
# https://git.ligo.org/lscsoft/gstlal/tree/master/gstlal-ugly/gst/framecpp
import struct
import logging


logger = logging.getLogger(__name__)

# hex dump: hexdump -s SKIP_BYTES -n BYTES_LONG -C, xxd -c 16, hd,
# od -x --skip-bytes=BYTES --read-bytes=BYTES
# xxd -c 32 /dev/shm/llhoft/H1/H-H1_llhoft-1208708235-1.gwf | less

FrameCPPConst = {
    "SIZEOF_FRHEADER": 40,
    "FRHEADER_IDENT": "IGWD",
    "SIZEOF_FRHEADER_IDENT": len("IGWD"),
    "FRSH_KLASS": 1,
    "FRENDOFFILE_NAME": "FrEndOfFile",
    "FRAMEH_NAME": "FrameH",
    # taken from /usr/include/math.h
    "M_PI": 3.14159265358979323846,
}


def frame_length(self, data):
    """
    Given an array of bytes which represent a frame, find the frame length.
    Uses Kipp Cannon's algorithms as described in framecpp_igwdparse.cc.
    These follow the data specs in the document:
    https://dcc.ligo.org/cgi-bin/DocDB/ShowDocument?docid=329 ."
    """

    #
    # offset points to the block of data we will examine next
    self.offset = 0
    #
    # filesize points to the end of the next block, at least as far as we know.
    # It is used to check to see if we've gone over the total length of the
    # data
    self.filesize = FrameCPPConst["SIZEOF_FRHEADER"]
    #
    # the reader points to what we're currently looking at. It starts at
    # offset and is increased until filesize
    self.reader = 0
    self.data_len = len(data)

    #
    # leave this code in place for debugging (for the moment, at least)
    self.endianness = None  # endianness. "<" = little endian, ">" = big endian
    self.version = None
    self.sizeof_int_2 = None
    self.sizeof_int_4 = None
    self.sizeof_int_8 = None
    self.sizeof_table_6 = None
    self.eof_klass = None
    self.frameh_klass = None
    self.file_start_time = None
    self.file_stop_time = None
    self.duration = None

    while self.filesize < self.data_len:
        if self.offset == 0:
            self.read_header(data)
            #
            # update pointers to the next thing we'll look at: a table 6
            self.offset = FrameCPPConst["SIZEOF_FRHEADER"]
            self.filesize = self.offset + self.sizeof_table_6
        else:
            self.parse_table_6_then_7(data)
            #
            # update pointers to start all over again, assuming next thing
            # should be a table 6
            self.offset = self.filesize
            self.filesize += self.sizeof_table_6

    return (self.file_start_time, self.file_stop_time, self.duration)


def fr_get_int_8u(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "Q", data[self.reader: self.reader + 8]
    )[0]
    if update_reader:
        self.reader += 8
    self.reader += post_skip
    return val


def fr_get_int_4u(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "L", data[self.reader: self.reader + 4]
    )[0]
    if update_reader:
        self.reader += 4
    self.reader += post_skip
    return val


def fr_get_int_2u(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "H", data[self.reader: self.reader + 2]
    )[0]
    if update_reader:
        self.reader += 2
    self.reader += post_skip
    return val


def fr_get_int_u(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "b", data[self.reader: self.reader + 1]
    )[0]
    if update_reader:
        self.reader += 1
    self.reader += post_skip
    return val


def fr_get_uint8(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "B", data[self.reader: self.reader + 1]
    )[0]
    if update_reader:
        self.reader += 1
    self.reader += post_skip
    return val


def fr_get_real4(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "f", data[self.reader: self.reader + 4]
    )[0]
    if update_reader:
        self.reader += 4
    self.reader += post_skip
    return val


def fr_get_real8(self, data, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    val = struct.unpack(
        self.endianness + "d", data[self.reader: self.reader + 8]
    )[0]
    if update_reader:
        self.reader += 8
    self.reader += post_skip
    return val


def fr_get_string(self, data, len, update_reader, pre_skip=0, post_skip=0):
    self.reader += pre_skip
    # only grab up  to len-1, do not grab the \0 (=^@) at the end
    val = data[self.reader: self.reader + len - 1]
    if update_reader:
        self.reader += len
    self.reader += post_skip
    return val


def read_header(self, data):
    if (
        data[0: FrameCPPConst["SIZEOF_FRHEADER_IDENT"]]
        != FrameCPPConst["FRHEADER_IDENT"]
    ):
        return "Error"
    # https://stackoverflow.com/questions/444591/
    # convert-a-string-of-bytes-into-an-int-python
    # https://docs.python.org/2/library/struct.html
    self.reader = 5
    # Temporarily set the endianness to "<" (little Endian), we will only read
    # bytes for a bit
    self.endianness = "<"
    self.version = self.fr_get_int_u(data, True, post_skip=1)
    if self.debug:
        logger.info("version: [", self.version, "]")
    self.sizeof_int_2 = self.fr_get_int_u(data, True)
    self.sizeof_int_4 = self.fr_get_int_u(data, True)
    self.sizeof_int_8 = self.fr_get_int_u(data, True, post_skip=1)

    #
    # validate
    if self.fr_get_int_u(data, True) != 8:
        if self.debug:
            logger.info(
                "Did not validate: should have found 8 = sizeof(REAL_8)"
            )

    # read in an int_2u using Little Endian and see what we get
    endian_check = self.fr_get_int_2u(data, True)
    if self.debug:
        logger.info("endian_check: [", endian_check, "]")
    # now see how it came in
    if endian_check == int("0x1234", 16):
        if self.debug:
            logger.info("Little endian")
        self.endianness = "<"
    elif endian_check == int("0x1234", 16):
        if self.debug:
            logger.info("Big endian")
        self.endianness = ">"
    else:
        if self.debug:
            logger.info("Could not determine endianness")

    if self.debug:
        logger.info("Checking endianness [", self.endianness, "]")
    if struct.unpack(
        self.endianness + "I", data[self.reader: self.reader + 4]
    )[0] != int("0x12345678", 16):
        if self.debug:
            logger.info("Failed 0x12345678 check...")
    self.reader += 4
    if struct.unpack(
        self.endianness + "Q", data[self.reader: self.reader + 8]
    )[0] != int("0x123456789abcdef", 16):
        if self.debug:
            logger.info("Failed 0x123456789abcdef check...")
    self.reader += 8
    #
    # note: rounding error because of
    # https://docs.python.org/2/library/struct.html
    # "For the 'f' and 'd' conversion codes, the packed representation uses
    # the IEEE 754 binary32 (for 'f') or binary64 (for 'd') format,
    # regardless of the floating-point format used by the platform.")
    pi_float = self.fr_get_real4(data, True)
    #
    # Now get a 4-byte version of PI
    # remember that Python "floats" are actually doubles in C
    M_PI_4 = struct.unpack(
        self.endianness + "f",
        struct.pack(self.endianness + "f", FrameCPPConst["M_PI"]),
    )[0]
    if self.debug:
        logger.info(
            "pi_float = [",
            pi_float,
            "], 4-byte float of pi should be: M_PI_4 = [",
            M_PI_4,
            "]",
        )
    if pi_float != M_PI_4:
        logger.info(
            "Failed float pi check: we got pi=[",
            pi_float,
            "], but should be: [",
            M_PI_4,
            "]",
        )
    pi_double = self.fr_get_real8(data, True)
    if self.debug:
        logger.info("pi_double: [", pi_double, "]")
    if self.debug:
        logger.info("M_PI=", FrameCPPConst["M_PI"])
    if pi_double != FrameCPPConst["M_PI"]:
        logger.info(
            "Failed double pi check: we got pi=[",
            pi_double,
            "], but should be: [",
            FrameCPPConst["M_PI"],
            "]",
        )

    self.sizeof_table_6 = (
        self.sizeof_int_8 + self.sizeof_int_2 + self.sizeof_int_4
    )
    if self.debug:
        logger.info("sizeof_table_6: [", self.sizeof_table_6, "]")

    self.eof_klass = 0
    self.frameh_klass = 0

    self.file_start_time = -1
    self.file_stop_time = 0
    self.duration = 0

    return True


def parse_table_6_then_7(self, data):
    if self.debug:
        logger.info("examining a table 6...")

    #
    # start our reader at the current offset
    self.reader = self.offset

    #
    # structure_length
    structure_length = self.fr_get_int_8u(data, True)
    if self.debug:
        logger.info("  structure_length: [", structure_length, "]")

    #
    # klass
    if self.version >= 8:
        self.klass = self.fr_get_uint8(
            data, True, pre_skip=1
        )  # (skip a byte, chkType)
    else:
        self.klass = self.fr_get_int_2u(data, True)
    if self.debug:
        logger.info("  klass: [", self.klass, "]")

    #
    # now that we've parsed a table 6, prepare to parse a table 7
    self.filesize = self.offset + structure_length

    if self.filesize > self.data_len:
        logger.info("Err: filesize > data_len")
        exit(1)

    #
    # now parse the table 7
    if self.debug:
        logger.info("   parsing table 7...")
    if self.klass == FrameCPPConst["FRSH_KLASS"] and (
        self.eof_klass == 0 or self.frameh_klass == 0
    ):
        if self.debug:
            logger.info("     FrSH structure")

        # now parse the table 7
        self.reader = self.offset + self.sizeof_table_6
        name_len = self.fr_get_int_2u(data, True)
        if self.debug:
            logger.info("     name_len: [", name_len, "]")
        name = self.fr_get_string(data, name_len, True)
        if self.debug:
            logger.info("     name: [", name, "]")
        if name == FrameCPPConst["FRENDOFFILE_NAME"]:
            self.eof_klass = self.fr_get_int_2u(data, True)
            if self.debug:
                logger.info("     eof_klass: [", self.eof_klass, "]")
        elif name == FrameCPPConst["FRAMEH_NAME"]:
            self.frameh_klass = self.fr_get_int_2u(data, True)
            if self.debug:
                logger.info("     frameh_klass: [", self.frameh_klass, "]")
        else:
            if self.debug:
                logger.info("     unknown name: [", name, "]")
    elif self.klass == self.frameh_klass:
        if self.debug:
            logger.info("      Found frameh_klass!")
        self.parse_table_9(data)
    elif self.klass == self.eof_klass:
        if self.debug:
            logger.info("       Found eof_klass!")
    else:
        if self.debug:
            logger.info("       Found something else")

    return True


#
# table 9 is where we get the start time and the duration
def parse_table_9(self, data):
    self.reader = self.offset + self.sizeof_table_6
    name_len = self.fr_get_int_2u(data, True)
    if self.debug:
        logger.info("         name_len: [", name_len, "]")
    name = self.fr_get_string(
        data, name_len, True, post_skip=3 * self.sizeof_int_4
    )
    if self.debug:
        logger.info("         name: [", name, "]")
    # the integer part of the start time
    start = self.fr_get_int_4u(data, True)
    # now the nanoseconds
    start += (
        self.fr_get_int_4u(data, True, post_skip=self.sizeof_int_2)
        * 0.000000001
    )
    if self.debug:
        logger.info("         start: [", start, "]")
    duration = self.fr_get_real8(data, True)
    if self.debug:
        logger.info("          duration: [", duration, "]")
    if duration > self.duration:
        self.duration = duration
    if start > self.file_start_time:
        self.file_start_time = start
    if start + round(duration) > self.file_stop_time:
        self.file_stop_time = start + round(duration)
    return True


if __name__ == "__main__":
    import sys

    for file_name in sys.argv[1:]:
        data = open(file_name, "rb").read()

        data_len = len(data)

        (frame_start_time, frame_stop_time, frame_duration) = frame_length(
            data
        )
        print(
            "(start, stop, duration): [",
            frame_start_time,
            frame_stop_time,
            frame_duration,
            "]",
        )
    exit(0)
