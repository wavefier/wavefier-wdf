# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022) and
# Laser Interferometer Gravitational-Wave Observatory (LIGO) (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#              Brockill <brockill@uwm.edu>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

import multiprocessing as mp
import threading
import logging
import os
import re
import signal
import time
import errno
from .framelen import frame_length
from .utils import check_lib_python3

logger = logging.getLogger(__name__)

############################################################
# process or thread which writes log files to disk
############################################################


class GracefulFrameLogKiller:
    kill_now = False
    signum = 0

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        logger.info(
            "framelog ^C: exiting gracefully. Will not notify \
the main thread/process."
        )
        # reset the signal handlers
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        signal.signal(signal.SIGTERM, signal.SIG_DFL)
        self.kill_now = True
        # save the signal
        self.signum = signum


class FakeKiller:
    kill_now = False


class MonitorThread:
    NONE = 0
    THREAD = 1
    PROCESS = 2


def frame_log(
    args,
    parent_pid,
    fl_partition,
    fl_frame_log_file_pattern,
    fl_ringn,
    fl_dyn_frame_len,
):

    logger.info(f"frame_log_thread started for partition [{fl_partition}]")
    if check_lib_python3("py3gdsio"):
        from py3gdsio import lsmp_read
        from py3gdsio import lsmp_kill
    elif check_lib_python3("llkafka.lsmp_read"):
        from llkafka import lsmp_read

        logger.info("frame_log_thread: imported llkafka/lsmp_read")
    elif check_lib_python3("lsmp_read"):
        import lsmp_read

        logger.info("frame_log_thread: imported lsmp_read")
    elif check_lib_python3("gds.pygdsio"):
        from gds import pygdsio

        lsmp_read = pygdsio
        logger.info("frame_log_thread: imported gds/pygdsio")

    from collections import deque

    if fl_ringn:
        file_name_dq = deque([], fl_ringn)

    #
    # Have we checked the directory exists (mkdir -p)?
    check_dir = False

    if args.frame_log_type == MonitorThread.PROCESS:
        # only exit gracefully if a process
        killer = GracefulFrameLogKiller()
    else:
        killer = FakeKiller()

    fl_reader = lsmp_read.ShmReader()
    while not fl_reader.connect(bytes(fl_partition, "utf-8")):
        time.sleep(0.1)

    logger.info(
        f"frame_log_thread: connected to partition [{fl_partition}] to read."
    )
    fl_last_ret = 0
    while fl_last_ret == 0 and not killer.kill_now:
        #
        # read data in from partition
        # This can generate an exception in the gds code, in particular:
        #     throw SysError("LSMP_CON::get_buffer consumer wait failed");
        # to handle this, we should follow the instructions here:
        # https://stackoverflow.com/questions/1394484/how-do-i-propagate\
        # -c-exceptions-to-python-in-a-swig-wrapper-library
        [fl_last_ret, read_data, fl_timestamp] = fl_reader.read_data()

        if fl_last_ret == 0:
            #
            # check crc
            if fl_reader.has_crc(read_data):
                if not fl_reader.check_crc(read_data):
                    logger.error("Error: CRC FAILED. Dropping.")
                    continue

            #
            # come up with a filename
            #
            file_name = re.sub(
                "%timestamp%",
                "%0.10d" % fl_timestamp,
                fl_frame_log_file_pattern,
            )

            #
            # do we have to calculate dynamically the length of the frame?
            if fl_dyn_frame_len:
                (
                    frame_start_time,
                    frame_stop_time,
                    frame_duration,
                ) = frame_length(read_data)
                file_name = re.sub(
                    "%delta_t%", str(int(frame_duration)), file_name
                )
                if args.debug > 0:
                    logger.debug(
                        " [debug, frame_log_thread, partition %s]: \
frame_start_time: [%f], frame_stop_time: [%f], frame_duration: [%f]\n"
                        % (
                            fl_partition,
                            frame_start_time,
                            frame_stop_time,
                            frame_duration,
                        )
                    )

            #
            # Do a 'mkdir -p' on the first file (only)
            if not check_dir:
                dir_name = os.path.dirname(file_name)
                if dir_name:
                    try:
                        os.makedirs(dir_name)
                    except OSError as exc:
                        if exc.errno == errno.EEXIST and os.path.isdir(
                            dir_name
                        ):
                            pass
                        else:
                            logger.error(
                                f"Error: could not create path [{dir_name}]"
                            )
                            raise
                check_dir = True

            #
            # write to disk
            with open(file_name, "wb") as fl_file:
                fl_file.write(read_data)
                fl_file.close()

                #
                # ring of frame_log files?
                if fl_ringn:
                    #
                    # name queue full?
                    if len(file_name_dq) == fl_ringn:
                        old_file = file_name_dq.popleft()
                        try:
                            os.unlink(old_file)
                        except OSError:
                            logger.info(
                                f"Error: could not delete file [{old_file}]"
                            )

                    #
                    # add this file to queue
                    file_name_dq.append(file_name)

            #
            # make sure parent still alive
            if args.frame_log_type == MonitorThread.PROCESS:
                if os.getppid() != parent_pid:
                    logger.info(
                        f"frame_log_thread: os.getppid()=[{os.getppid()}] != \
parent_pid=[{parent_pid}], parent process killed, so stopping."
                    )
                    exit(1)

        else:
            #
            # got an error while trying to read from the shm partition
            logger.info(
                "Got an error reading from the shared memory partition: \
[{fl_last_ret}]"
            )

    logger.info(f"Error: frame_log_thread([{fl_partition}]) stopping.")

    if args.kill_lsmp_on_exit:
        lsmp_killer = lsmp_kill.ShmKill()
        logger.info(
            "frame_log_thread: deleting shared object writer for shared \
memory [{fl_partition}]"
        )
        lsmp_killer.kill(bytes(fl_partition, "utf-8"))

    exit(1)


############################################################
# Start up processes, threads
############################################################
#
# get the pid of this process
parent_pid = os.getpid()


def frame_log_subst(tp_info, topic, partition, frame_log_file_pattern):
    #
    # do some initial substitutions on the file name
    frame_log_file_pattern_partial = re.sub(
        "%partition%", partition, frame_log_file_pattern
    )
    #
    # if not calculating frame lengths dynamically
    if "delta_t" in tp_info[topic] and tp_info[topic]["delta_t"]:
        frame_log_file_pattern_partial = re.sub(
            "%delta_t%",
            str(tp_info[topic]["delta_t"]),
            frame_log_file_pattern_partial,
        )
    if "ifo" in tp_info[topic] and tp_info[topic]["ifo"]:
        frame_log_file_pattern_partial = re.sub(
            "%ifo%", tp_info[topic]["ifo"], frame_log_file_pattern_partial
        )
    if "obs" in tp_info[topic] and tp_info[topic]["obs"]:
        frame_log_file_pattern_partial = re.sub(
            "%obs%", tp_info[topic]["obs"], frame_log_file_pattern_partial
        )
    else:
        if "ifo" in tp_info[topic] and tp_info[topic]["ifo"]:
            frame_log_file_pattern_partial = re.sub(
                "%obs%",
                tp_info[topic]["ifo"][0].upper(),
                frame_log_file_pattern_partial,
            )
    return frame_log_file_pattern_partial


def frame_log_process(args, tp_info):
    fl_p = {}
    for topic in tp_info:
        partition = tp_info[topic]["partition"]
        frame_log_file_pattern_partial = frame_log_subst(
            tp_info, topic, partition, args.frame_log_file_pattern
        )
        if "ringn" in tp_info[topic] and tp_info[topic]["ringn"]:
            ringn_loc = int(tp_info[topic]["ringn"])
        else:
            if args.ringn:
                ringn_loc = int(args.ringn)
            else:
                ringn_loc = None
        #
        # do we have to dynamically calculate frame length?
        dyn_frame_len = False
        if not ("delta_t" in tp_info[topic]) or not tp_info[topic]["delta_t"]:
            if (
                "delta_t_fallback" in tp_info[topic]
                and tp_info[topic]["delta_t_fallback"]
            ):
                dyn_frame_len = True
        if args.debug is not None and args.debug > 0:
            if dyn_frame_len:
                logger.info(
                    "Dynamically calculating frame lengths for topic [%s]\n"
                    % topic
                )
            else:
                logger.info(
                    "Will NOT be dynamically calculating frame lengths for \
topic [%s]\n"
                    % topic
                )

        fl_p[topic] = mp.Process(
            target=frame_log,
            args=(
                args,
                parent_pid,
                partition,
                frame_log_file_pattern_partial,
                ringn_loc,
                dyn_frame_len,
            ),
        )
        fl_p[topic].start()
    logger.info(
        f"main: launched frame_log_thread as a process, child pids: \
[{tp_info[topic]['partition']},{fl_p[topic].pid}) for topic in tp_info], ], \
parent_pid: [{parent_pid}]"
    )

    return fl_p


def frame_log_thread(tp_info, ringn, frame_log_file_pattern):
    fl_p = {}
    for topic in tp_info:
        partition = tp_info[topic]["partition"]
        frame_log_file_pattern_partial = frame_log_subst(
            topic, partition, frame_log_file_pattern
        )
        if "ringn" in tp_info[topic] and tp_info[topic]["ringn"]:
            ringn_loc = int(tp_info[topic]["ringn"])
        else:
            if ringn:
                ringn_loc = int(ringn)
            else:
                ringn_loc = None
        fl_p[topic] = threading.Thread(
            target=frame_log_thread,
            args=(
                parent_pid,
                partition,
                frame_log_file_pattern_partial,
                ringn_loc,
            ),
        )
        fl_p[topic].daemon = True
        fl_p[topic].start()
    partitions = [tp_info[topic]["partition"] for topic in tp_info]
    logger.info(
        f"main: launched frame_log_thread as a thread for partitions: \
[{partitions}], parent_pid: [{parent_pid}]"
    )

    return fl_p
