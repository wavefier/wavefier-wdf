#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022) and
# Laser Interferometer Gravitational-Wave Observatory (LIGO) (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#              Brockill <brockill@uwm.edu>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

import sys
import configargparse
import importlib
import logging
from .utils import str2bool, check_crc
from .framekafkaproducer import FrameKafkaProducer

logger = logging.getLogger()
logger.setLevel("INFO")
# right now we're having trouble having these logs go to the pipes
# we set up before: we get a bunch of errors in the logging module
# when it tries to call self.stream.flush(), probably because of
# the O_NONBLOCKING option used when opened
# consoleHandler = logging.StreamHandler(f_log.get_file())
consoleHandler = logging.StreamHandler()
logger.addHandler(consoleHandler)


def main():

    parser = configargparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", is_config_file=True, help="config file path"
    )
    parser.add_argument(
        "-p",
        "--partition",
        type=str,
        required=True,
        help="Partition to read from",
    )
    parser.add_argument(
        "-d",
        "--debug",
        type=int,
        default=0,
        help="increase debug level (default 0=off)",
    )
    parser.add_argument(
        "-b",
        "--bootstrap-servers",
        type=str,
        default="localhost:9092",
        help="Specify the Kafka cluster bootstrap servers",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        const=True,
        default=False,
        help="verbose log output",
    )
    parser.add_argument("-g", "--group-id", type=str, help="Kafka group ID")
    parser.add_argument(
        "-S",
        "--split-bytes",
        type=int,
        default=100000,
        help="Split messages into this many bytes when adding to Kafka",
    )
    parser.add_argument(
        "-t", "--topic", type=str, help="Kafka topic to write to"
    )
    parser.add_argument(
        "-crc",
        "--crc-check",
        type=str2bool,
        default=True,
        help="Run a CRC check for each frame",
    )
    parser.add_argument(
        "-bs",
        "--batch-size",
        type=int,
        help="Change the batch.size for the producer [default: 16384]",
    )
    parser.add_argument(
        "-bm",
        "--buffer-memory",
        type=int,
        help="Change the buffer.memory for the producer \
[default: 33554432B=32MB]",
    )
    parser.add_argument(
        "-lm",
        "--linger-ms",
        type=int,
        help="Change the linger.ms for the producer [default: 0]",
    )
    parser.add_argument(
        "-a",
        "--acks",
        default=1,
        type=int,
        help="Change the acks for the producer",
    )
    parser.add_argument(
        "-mi",
        "--min-interval",
        default=-1,
        type=float,
        help="Enforce a minimum interval between gwf files",
    )
    parser.add_argument(
        "-su",
        "--status-updates",
        action="store_const",
        const=True,
        default=False,
        help="store status updates",
    )
    parser.add_argument(
        "-st", "--status-topic", type=str, help="topic name for status updates"
    )
    parser.add_argument(
        "-sb",
        "--status-bootstrap",
        type=str,
        help="specify the kafka cluster for status updates",
    )
    parser.add_argument(
        "-si",
        "--status-interval",
        type=int,
        default=60,
        help="interval in seconds between status updates",
    )
    parser.add_argument(
        "-dt",
        "--delta-t",
        default=1,
        type=int,
        help="Make sure each frame comes in at delta_t seconds",
    )
    # custom libraries
    parser.add_argument(
        "-lk",
        "--lsmp-kafka-dir",
        default=None,
        type=str,
        help="directory where lsmp_read is located",
    )
    parser.add_argument(
        "-lkp",
        "--load-kafka-python",
        action="store_const",
        const=True,
        default=False,
        help="load kafka-python rather than confluent-kafka",
    )

    # Parse the arguments from the command line
    args = parser.parse_args()

    # check if module exists before importing it
    # we'll have to rewrite this for python3, see the following link:
    # https://stackoverflow.com/questions/14050281/
    # how-to-check-if-a-python-module-exists-without-importing-it
    def check_lib_python3(libname):
        logger.info(f"Looking for library [{libname}]...")
        loadlib = importlib.util.find_spec(libname)
        if loadlib is not None:
            return True
        else:
            return False

    if args.lsmp_kafka_dir:
        sys.path.insert(0, args.lsmp_kafka_dir)
    # FIXME: when run the next command, actually loads gds (parent)
    # when checking
    if check_lib_python3("py3gdsio"):
        from py3gdsio import lsmp_read
    elif check_lib_python3("lsmp_read"):
        import lsmp_read

        logger.info("import lsmp_read... OK")
    else:
        logger.info(
            "Error: could not find llkafka/lsmp_read or just lsmp_read. Use \
--lsmp-kafka-dir to specify."
        )
        exit(1)

    logger.info(f"partition: [{args.partition}]")
    logger.info(f"verbose: [{args.verbose}]")
    logger.info(f"topic: [{args.topic}]")
    logger.info(f"crc_check: [{args.crc_check}]")
    logger.info(f"bootstrap_servers: [{args.bootstrap_servers}]")
    logger.info(f"split_bytes: [{args.split_bytes}]")
    logger.info(f"batch_size: [{args.batch_size}]")
    logger.info(f"linger_ms: [{args.linger_ms}]")
    logger.info(f"min_interval: [{args.min_interval}]")
    logger.info(f"delta_t: [{args.delta_t}]")

    framekafkaproducer = FrameKafkaProducer(args)
    ############################################################
    # Connect to Shared memory partition for reading
    ############################################################
    reader = lsmp_read.ShmReader()
    if not reader.connect(bytes(args.partition, "utf-8")):
        logger.info(
            f"Error: could not open partition [{args.partition}] for reading."
        )
        framekafkaproducer.close()
        return 1

    last_ret = 0
    while last_ret == 0:

        # Get the data from the shared memory partition
        [last_ret, data, timestamp] = reader.read_data()

        # Check if the data has a Cyclic Redundancy Check (crc) and if it pass
        if args.crc_check:
            returncode = check_crc(
                reader, args.topic, data, timestamp, args.partition
            )

            if returncode != 0:
                continue

        # Send the frame
        framekafkaproducer.send_frame(data, timestamp)

        if last_ret != 0:
            logger.info(
                "Error reading from shared memory partition. Ignoring."
            )
            last_ret = 0

    # Close the producer
    framekafkaproducer.close()


if __name__ == "__main__":
    sys.exit(main())
