from igwn_lldd.framefilemonitor import frame_log_subst


def test_frame_log_subst():

    # Set the test topics
    topic = "test"

    # Set the partition
    partition = "TestPartition1"

    # The frame log file pattern
    frame_log_file_pattern = "%obs%-%ifo%_llhoft-%timestamp%-%delta_t%.gwf"

    # The expected frame log file pattern
    frame_log_file_pattern_exp = "Z-Z1_llhoft-%timestamp%-1.gwf"

    # Create the topic dictionary
    topic_dict = {topic: {}}

    # Populate the first topic with relevant values
    topic_dict[topic]["delta_t"] = int(1)
    topic_dict[topic]["ifo"] = "Z1"

    # Test the frame pattern substitution
    frame_log_file_pattern_ret = frame_log_subst(
        topic_dict,
        topic,
        partition,
        frame_log_file_pattern
    )

    # Check the returned is the same as the expected
    assert frame_log_file_pattern_ret == frame_log_file_pattern_exp, (
        "The returned frame log file pattern is not the same as the"
        "expected pattern:\n"
        f"\t{frame_log_file_pattern_ret} != {frame_log_file_pattern_exp}"
    )

    # Add the observatory to the topic dictionary
    topic_dict[topic]["obs"] = "Z"

    # Test the frame pattern substitution with the observatory
    frame_log_file_pattern_ret = frame_log_subst(
        topic_dict,
        topic,
        partition,
        frame_log_file_pattern
    )

    # Check the returned is the same as the expected
    assert frame_log_file_pattern_ret == frame_log_file_pattern_exp, (
        "The returned frame log file pattern is not the same as the"
        "expected pattern:\n"
        f"\t{frame_log_file_pattern_ret} != {frame_log_file_pattern_exp}"
    )
