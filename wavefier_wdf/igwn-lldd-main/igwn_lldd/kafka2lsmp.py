#!/usr/bin/python3

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022) and
# Laser Interferometer Gravitational-Wave Observatory (LIGO) (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#              Brockill <brockill@uwm.edu>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import errno
import time
import configargparse
import logging
import inspect
from .utils import (
    parse_topics_lsmp,
    GracefulKiller,
    str2bool,
    check_lib_python3,
    # check_crc,
)
from .framefilemonitor import (
    MonitorThread,
    frame_log_process,
    frame_log_thread,
)
from .framekafkaconsumer import FrameKafkaConsumer

logger = logging.getLogger()
logger.setLevel("INFO")
consoleHandler = logging.StreamHandler()
logger.addHandler(consoleHandler)


def str2spigot(v):
    if v.lower() in ("none", "0"):
        return MonitorThread.NONE
    elif v.lower() in ("thread"):
        return MonitorThread.THREAD
    elif v.lower() in ("process"):
        return MonitorThread.PROCESS
    else:
        raise configargparse.ArgumentTypeError(
            'Expected "none", "thread" or "process" for spigot check type'
        )


def main():

    parser = configargparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", is_config_file=True, help="config file path"
    )
    parser.add_argument(
        "-d", "--debug", type=int, help="increase debug level (default 0=off)"
    )
    parser.add_argument(
        "-dw",
        "--debug-wait",
        type=float,
        default=0.0,
        help="wait x seconds between gwf files (used to intentionally break \
things for debugging)",
    )
    parser.add_argument(
        "-b",
        "--bootstrap-servers",
        type=str,
        default="localhost:9092",
        help="specify the Kafka cluster bootstrap servers",
    )
    parser.add_argument(
        "-a",
        "--add-topic-partition",
        type=str,
        action="append",
        help="/topic=LHO_Data/partition=KLHO_Data/nbuf=16/lbuf=1000000/\
delta-t=4/crc-check=true/",
    )
    parser.add_argument(
        "-x",
        "--exit-if-missing-topics",
        action="store_const",
        const=True,
        default=False,
        help="exit if any topics are missing",
    )
    parser.add_argument(
        "-s",
        "--ssl",
        action="store_const",
        const=True,
        default=False,
        help="use ssl",
    )
    parser.add_argument("-p", "--ssl-password", type=str, help="ssl password")
    parser.add_argument(
        "-ca", "--ssl-cafile", type=str, help="location of ca-cert"
    )
    parser.add_argument(
        "-cf",
        "--ssl-certfile",
        type=str,
        help="location of signed certificate",
    )
    parser.add_argument(
        "-kf", "--ssl-keyfile", type=str, help="location of personal keyfile"
    )
    parser.add_argument("-g", "--group-id", type=str, help="Kafka group ID")
    parser.add_argument(
        "-su",
        "--status-updates",
        action="store_const",
        const=True,
        default=False,
        help="store status updates",
    )
    parser.add_argument(
        "-st", "--status-topic", type=str, help="topic name for status updates"
    )
    parser.add_argument(
        "-sb",
        "--status-bootstrap",
        type=str,
        help="specify the kafka cluster for status updates",
    )
    parser.add_argument(
        "-si",
        "--status-interval",
        type=int,
        default=60,
        help="interval in seconds between status updates",
    )
    parser.add_argument(
        "-sn",
        "--status-nodename",
        type=str,
        help="specify the node name used in status updates",
    )
    parser.add_argument(
        "-ff",
        "--fast-forward",
        type=str2bool,
        default=True,
        help="fast forward if fall behind",
    )
    # custom libraries
    parser.add_argument(
        "-lk",
        "--lsmp-kafka-dir",
        default=None,
        type=str,
        help="Directory where lsmp_write is located",
    )
    parser.add_argument(
        "-lkp",
        "--load-kafka-python",
        action="store_const",
        const=True,
        default=False,
        help="load kafka-python rather than confluent-kafka",
    )
    parser.add_argument(
        "-klsmp",
        "--kill-lsmp-on-exit",
        type=str2bool,
        default=True,
        help="Explicitly kill shared memory partitions on exit",
    )
    parser.add_argument(
        "-fl",
        "--frame-log-type",
        type=str2spigot,
        default=MonitorThread.NONE,
        help="Turn on frame logging ('none', 'thread' or 'process')",
    )
    parser.add_argument(
        "-flp",
        "--frame-log-file-pattern",
        type=str,
        default=None,
        help="Path where frames will be saved: %%obs%%, %%partition%%, etc.",
    )
    parser.add_argument(
        "-rn",
        "--ringn",
        type=int,
        default=None,
        help="Number of frame_log files to retain",
    )
    parser.add_argument(
        "-pt",
        "--poll-timeout",
        type=int,
        default=1000,
        help="Timeout when doing consumer.poll() [in ms]. Default: 1000.",
    )
    parser.add_argument(
        "-pr",
        "--poll-max_records",
        type=int,
        default=1,
        help="Max records returned when doing consumer.poll(). Default: 1.",
    )
    parser.add_argument(
        "-pif",
        "--pid-file",
        type=str,
        default=None,
        help="File in which to store PID of main process.",
    )
    parser.add_argument(
        "-sbm",
        "--shm-buffer-mode",
        type=int,
        default=5,
        help="Shm buffer mode for shared memory, default: 5=release,scavage.",
    )
    parser.add_argument(
        "-rsmr",
        "--run-smrepair",
        action="store_const",
        const=True,
        default=False,
        help="Run /usr/bin/smrepair --bufmode [shm-buffer-mode] within the \
frame_logger processes.",
    )
    parser.add_argument(
        "-ffpc",
        "--force-param-connect",
        type=int,
        default=-1,
        help="Force the four- or five-parameter ShmWriter.connect() function \
to be called by setting this to 4 or 5.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=False,
        const=True,
        action="store_const",
        help="verbose log output",
    )

    # Parse the arguments from the command line
    args, unknown = parser.parse_known_args()

    # Get the topics from the topic partition
    tp_info = parse_topics_lsmp(args.add_topic_partition)

    # Check the arguments
    if (
        args.frame_log_file_pattern
        and args.frame_log_type == MonitorThread.NONE
    ):
        raise ValueError(
            "Need to specify --frame-log-type=thread or \
--frame-log-type=process if using --frame-log-file-pattern"
        )

    if (
        not args.frame_log_file_pattern
        and args.frame_log_type != MonitorThread.NONE
    ):
        raise ValueError(
            "Need to specify --frame-log-file-pattern if \
 --frame-log-type=process or =thread is used."
        )

    # FIX: should just be the unique dirs amongst these
    if args.lsmp_kafka_dir:
        sys.path.insert(0, args.lsmp_kafka_dir)
    # FIXME: when run the next command, actually loads gds (parent) when
    # checking
    # if check_lib_python3('gds.pygdsio'):
    #     from gds import pygdsio
    #     lsmp_write=pygdsio
    #     lsmp_kill=pygdsio
    if check_lib_python3("py3gdsio"):
        from py3gdsio import lsmp_write

        logger.info("from py3gdsio import lsmp_write... OK")
        from py3gdsio import lsmp_kill

        logger.info("from py3gdsio import lsmp_kill... OK")
    elif check_lib_python3("lsmp_write"):
        import lsmp_write

        logger.info("import lsmp_write... OK")
        import lsmp_kill

        logger.info("import lsmp_kill... OK")

    # Create the PID file if requested
    if args.pid_file is not None:
        try:
            with open(args.pid_file, "w") as f:
                f.write("%d\n" % os.getpid())
            f.close()
        except IOError as e:
            logger.error(
                "Error opening PID file [",
                args.pid_file,
                "]. errno: ",
                e.errno,
                ", strerror: [",
                e.strerror,
                "]",
            )
            if e.errno == errno.EACCES:
                raise IOError(
                    "No permission to open PID file [", args.pid_file, "]"
                )
            elif e.errno == errno.EISDIR:
                raise IOError(
                    "Cannot write PID file [",
                    args.pid_file,
                    "], since this is a directory.",
                )

    # Startup the frame kafka consumer
    framekafkaconsumer = FrameKafkaConsumer(args, tp_info)

    # Frame Logging process
    # For the moment, it seems this only works for processes, not threads
    if args.frame_log_type == MonitorThread.PROCESS:
        fl_p = frame_log_process(args, tp_info)
    elif args.frame_log_type == MonitorThread.THREAD:
        fl_p = frame_log_thread(
            tp_info, args.ringn, args.frame_log_file_pattern
        )

    # Capture signals in the main thread
    killer = GracefulKiller()

    ############################################################
    # Shared memory writers
    ############################################################
    #
    # temporary measure:
    # check to see which version of ShmWriter.connect() we have
    shmwriter_connect_paramnums = len(
        inspect.getargspec(lsmp_write.ShmWriter.connect)[0]
    )
    if args.force_param_connect > 0:
        shmwriter_connect_paramnums = args.force_param_connect
    if shmwriter_connect_paramnums == 4:
        logger.info(
            "Using original 4-parameter version of ShmWriter.connect()"
        )
    elif shmwriter_connect_paramnums == 5:
        logger.info(
            "Using 5-parameter version of ShmWriter.connect() with buffer_mode"
        )
    else:
        logger.error(
            f"Error: number of parameters [{shmwriter_connect_paramnums}] \
    should only be 4 or 5 for lsmp_write.ShmWriter.connect()"
        )
        return 1

    for topic in tp_info.keys():
        w = lsmp_write.ShmWriter()

        connect_ret = False
        if shmwriter_connect_paramnums == 4:
            connect_ret = w.connect(
                bytes(tp_info[topic]["partition"], "utf-8"),
                tp_info[topic]["nbuf"],
                tp_info[topic]["lbuf"],
            )
        elif shmwriter_connect_paramnums == 5:
            connect_ret = w.connect(
                tp_info[topic]["partition"],
                tp_info[topic]["nbuf"],
                tp_info[topic]["lbuf"],
                args.shm_buffer_mode,
            )

        if not connect_ret:
            logger.error(
                "Error: could not open partition [%s] with nbuf [%d] and \
lbuf [%d] for write"
                % (
                    tp_info[topic]["partition"],
                    tp_info[topic]["nbuf"],
                    tp_info[topic]["lbuf"],
                )
            )
            logger.error(
                f"  connect param nums: [{shmwriter_connect_paramnums}]"
            )
            return -1
        tp_info[topic]["lsmp_writer"] = w

        #
        # run /usr/bin/smrepair to fix the partition
        # note that this has to be done *after* an ShmReader has been
        # connected, otherwise running smrepair will kill the partition
        if args.run_smrepair:
            import subprocess

            # redirect output
            # dev_null = open(os.devnull, 'w')
            # ret = subprocess.call(['/usr/bin/smrepair', '--bufmode',
            # str(buffer_mode), topic], stdout=dev_null,
            # stderr=subprocess.STDOUT)
            smrepair_cmd = [
                "/usr/bin/smrepair",
                "--bufmode",
                str(args.shm_buffer_mode),
                tp_info[topic]["partition"],
            ]
            logger.info("Running command: [%s] ..." % (" ".join(smrepair_cmd)))
            subprocess.call(smrepair_cmd)

    while not killer.kill_now:

        # Run consumer.poll()
        r_poll = framekafkaconsumer.poll_cosumer_for_topic()

        # parse the messages
        for topic_partition in r_poll:
            for message in r_poll[topic_partition]:

                # We can check for ^C frequently, it's just a variable
                if killer.kill_now:
                    logger.info("main: ^C while processing messages")
                    break

                # Get the frame buffer from the kafka messgae
                (
                    frame_buffer,
                    payload_info,
                ) = framekafkaconsumer.extract_frame_buffer_from_message(
                    message, tp_info
                )

                # Continue if the full frame has not been assembled
                if not frame_buffer:
                    continue

                # now write this to the shared memory partition
                # FIXME
                writer = tp_info[payload_info["topic"]]["lsmp_writer"]
                last_ret = writer.write_data(
                    frame_buffer, payload_info["data_gps_timestamp"]
                )

                if args.debug_wait > 0.0:
                    logger.info("WAITING...")
                    time.sleep(args.debug_wait)

                if last_ret != 0:
                    logger.info(
                        "Topic: [%s] Partition: [%s] %s \
write_data returned: [%d]\n"
                        % (
                            payload_info["topic"],
                            tp_info[payload_info["topic"]]["partition"],
                            time.asctime(),
                            last_ret,
                        )
                    )

    logger.info("main: shutting down")
    if args.frame_log_type == MonitorThread.PROCESS:
        if args.frame_log_file_pattern:
            for topic in fl_p:
                logger.info(
                    "main: terminating frame_log_thread for topic [{topic}]"
                )
                fl_p[topic].terminate()

    # and close the Kafka connection
    framekafkaconsumer.close()

    # explicitly kill shared memory partitions before
    # calling a signal handler below
    if args.kill_lsmp_on_exit:
        lsmp_killer = lsmp_kill.ShmKill()
        for topic in tp_info:
            if tp_info[topic]["lsmp_writer"]:
                if tp_info[topic]["partition"]:
                    logger.info(
                        f"Deleting shared object writer for topic [{topic}] \
and shared memory [{tp_info[topic]['partition']}]"
                    )
                    lsmp_killer.kill(
                        bytes(tp_info[topic]["partition"], "utf-8")
                    )

    if args.pid_file is not None:
        logger.info("Removing old pid file [", args.pid_file, "]")
        os.unlink(args.pid_file)

    # did we exit out on a signal?
    # if so, we've reset the signal handlers, so call the signal
    # again to get the default handling.
    # (If we don't do this, then killing the parent by using "kill"
    # and not "kill -9" will just keep the parent around waiting
    # for the child to terminate, for example.)
    if killer.kill_now:
        os.kill(os.getpid(), killer.signum)


if __name__ == "__main__":
    sys.exit(main())
