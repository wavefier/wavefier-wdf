#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (EGO) (2022) and
# Laser Interferometer Gravitational-Wave Observatory (LIGO) (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#              Brockill <brockill@uwm.edu>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.


# Transcribed from the original written in php by tzs@andrews.edu:
#   https://www.andrews.edu/~tzs/timeconv/timealgorithm.html
# to Python by Edward Biggs
#
#
# The following links may also be of interest:
# [1] https://stackoverflow.com/questions/44274989/
#     utc-time-to-gps-time-converter-by-python
# [2] https://stackoverflow.com/questions/9227888/gps-time-time-conversion
# [3] https://www.andrews.edu/~tzs/timeconv/timeconvert.php
# [4] https://www.andrews.edu/~tzs/timeconv/timealgorithm.html
# [5] https://www.google.com/search?q=python+convert+gps+time
# [6] https://en.wikipedia.org/wiki/Global_Positioning_System#Leap_seconds
# [7] https://stackoverflow.com/questions/20521750/
#     ticks-between-unix-epoch-and-gps-epoch
# [8] https://en.wikipedia.org/wiki/Leap_second#Insertion_of_leap_seconds
# [9] https://www.timeanddate.com/time/leap-seconds-future.html

#
# For the moment, only UNIX2GPS and GPS2UNIX, but we could imagine there
# being others
class TimeConvertDir:
    UNIX2GPS = 1
    GPS2UNIX = 2


#
# https://stackoverflow.com/questions/6180185/
# custom-python-exceptions-with-error-codes-and-error-messages
class TimeConvertError(Exception):
    def __init__(self, *args):
        self.args = [a for a in args]

    def __str__(self):
        return repr(self.code)


#
# leap seconds
# These must be periodically updated as new leap seconds are announced! :P
def getleaps():
    leaps = [
        46828800,
        78364801,
        109900802,
        173059203,
        252028804,
        315187205,
        346723206,
        393984007,
        425520008,
        457056009,
        504489610,
        551750411,
        599184012,
        820108813,
        914803214,
        1025136015,
        1119744016,
        1167264017,
    ]
    return leaps


def isleap(gpsTime):
    isleap = 0
    leaps = getleaps()
    for i in leaps:
        if gpsTime == i:
            isleap = 1
    return isleap


def countleaps(gpsTime, dirFlag):
    leaps = getleaps()
    nleaps = 0
    for i in leaps:
        if dirFlag == TimeConvertDir.UNIX2GPS:
            if gpsTime >= i:
                nleaps += 1
        elif dirFlag == TimeConvertDir.GPS2UNIX:
            if gpsTime >= i:
                nleaps += 1
        else:
            raise TimeConvertError(
                1,
                "Invalid flag",
                "The flag was neither TimeConvertDir.UNIX2GPS nor \
TimeConvertDir.GPS2UNIX",
            )
    return nleaps


def unix2gps(unixTime):
    if isinstance(unixTime, int):
        unixTime = unixTime - 0.5
        isLeap = 1
    else:
        isLeap = 0
    gpsTime = unixTime - 315964800
    nleaps = countleaps(gpsTime, TimeConvertDir.UNIX2GPS)
    gpsTime = gpsTime + nleaps + isLeap
    return gpsTime


def gps2unix(gpsTime):
    unixTime = gpsTime + 315964800
    nleaps = countleaps(gpsTime, TimeConvertDir.GPS2UNIX)
    unixTime = unixTime - nleaps
    if isleap(gpsTime):
        unixTime = unixTime + 0.5
    return unixTime


if __name__ == "__main__":
    import sys
    import time

    unixTime = None
    gpsTime = None

    direction_string = sys.argv[1]
    timeinput = sys.argv[2]
    if direction_string == "gps2unix":
        direction = TimeConvertDir.GPS2UNIX
        try:
            timeinput = float(sys.argv[2])
        except ValueError:
            timeinput = unix2gps(time.time())
            print("(Using current gps value for time: [", timeinput, "])")
        gpsTime = float(timeinput)
    else:
        direction = TimeConvertDir.UNIX2GPS
        try:
            timeinput = float(sys.argv[2])
        except ValueError:
            timeinput = time.time()
            print("(Using current unix value for time: [", timeinput, "])")
        unixTime = float(timeinput)

    if direction == TimeConvertDir.GPS2UNIX:
        unixTime = gps2unix(gpsTime)
        print("Unix Time: [%0.6f]" % unixTime)
    else:
        gpsTime = unix2gps(unixTime)
        print("GPS Time: [%0.6f]" % gpsTime)
