from pytsa.tsa import SeqView_double_t
import logging

# TODO: fix "downsamplig" typo in the future to the "Downsampling"
from wdf.processes.filtering import downsamplig as Downsampling
from wdf.config.DownsampParameters import DownsampParameters

class ApplyDownsampling:
    """
    The following class contains the method applying downsampling to the Sequence View data
    """

    def __init__(self, dwnsmp_parameters: DownsampParameters):
        """
        The following class contains the method applying downsampling to the Sequence View data

        :type dwnsmp_parameters: DownsampParam
        :param dwnsmp_parameters: Set of parameters for downsampling
        """
        self.dwnsmp_parameters = dwnsmp_parameters
        self.ds = Downsampling(self.dwnsmp_parameters)

    def process(self, sv: SeqView_double_t) -> SeqView_double_t:
        """
        Applies downsampling procedure to the Sequence View data
        :type sv: SeqView_double_t
        :param sv: Sequence View object

        :return: Downsampled Sequence View object (if possible) and the status of downsampling procedure (failed or success)
        """
        # Downsample data if possible
        logging.info("Downsampling data")
        downsampled_sv = SeqView_double_t()
        self.ds.Process(sv, downsampled_sv)
        
        # ds_status = self.ds.Process(sv, downsampled_sv)
        #if ds_status:
        if sv.GetSampling() != downsampled_sv.GetSampling():
            logging.info("Successful downsampling")
            sv = downsampled_sv
        else:
            logging.warning("Downsampling failed, whitening anyway")

        return sv
