from pytsa.tsa import SeqView_double_t
from os import path
import logging
from wdf.processes.whitening import Whitening

from wavefier_wdf.preprocessing.ApplyDownsampling import ApplyDownsampling

from wdf.config.WhtParameters import WhtParameters
from wdf.config.WdfParameters import WdfParameters     

class ApplyWhitening:
    """
    The following class contains the method applying whitening to the Sequence View data
    Later version will use Adaptive filtering so there won't be necessity to create file with AR and LV parameters.
    """
    def __init__(self, wdf_parameters: WdfParameters, w_parameters: WhtParameters):
        """
        The following class contains the method applying whitening to the Sequence View data
        :type wdf_parameters: WdfParam
        :param wdf_parameters: Set of WDF parameters

        :type w_parameters: WitheningParam
        :param w_parameters: Set of whitening parameters
        """
        self.wdf_parameters = wdf_parameters
        self.w_parameters = w_parameters

        # Initialize downsampling
        logging.info("Initialize downsampling...")
        self.ds = ApplyDownsampling(self.wdf_parameters, self.w_parameters)

        # Initialize Whitening for the given parameters
        logging.info("Initialize whitening: Load AR and LV parameters...")
        self.whiten = Whitening(self.w_parameters.AR)
        tmp_path = path.dirname(__file__) + "/"
        self.whiten.ParametersLoad(tmp_path + "sample_AR.txt", tmp_path + "sample_LV.txt")

    def process(self, sv: SeqView_double_t) -> SeqView_double_t:
        """
        Applies whitening methods on the Sequence View data
        :type sv: SeqView_double_t
        :param sv: Sequence View object

        :return: Whitened Sequence View object and the sigma parameter of whitening
        """
        # Downsample data if possible, before applying whitening
        sv = self.ds.process(sv)

        # Apply whitening to the data
        logging.info("Whitening SV data...")
        whitened_sv = SeqView_double_t()
        self.whiten.Process(sv, whitened_sv)

        # Get the Sigma value of the Whitening procedure
        sigma = self.whiten.GetSigma()

        # Update wdf parameters by adding sigma value
        self.wdf_parameters.__setattr__("sigma", sigma)

        return whitened_sv
