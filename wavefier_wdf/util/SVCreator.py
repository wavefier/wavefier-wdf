from pytsa.tsa import SeqView_double_t as SV
from pytsa.tsa import SeqView_double_t
from pytsa.tsa import FrameIChannel
import logging

from wavefier_wdf.util.Update import Update
from wavefier_common.RawData import RawData
from wavefier_common.param.WdfParam import WdfParam
from wavefier_common.param.WitheningParam import WitheningParam

import os

class SVCreator(object):

    def generate(self, stream_data: RawData, channel: str) -> SeqView_double_t:
        """
        Generate the Sequence View data from the serializer containing stream of binary data and gps time

        Since FrameIChannel requires path to the data, not the stream, temporary file will be created.

        :type channel: str
        :param channel: string with the name of the channel to analyse

        :return: pytsa.SeqViewDouble
        """

        # Save the data into temporary file
        #gps = stream_data.getStartGPSTime()
        gps = 0.0
        stream = stream_data.getData()
        gps = stream_data.getStartGPSTime()
        tmp_name = "data_" + str(gps) + ".gwf"
        with open(tmp_name, "wb") as g:
            g.write(stream)

        print(channel)
        print(tmp_name)
        # Reading the data from the temporary file
        logging.info("Reading frame data")
        ## TODO REVIEW : channel =  os.environ["CHANNEL"]
        channel =  os.environ["CHANNEL"]
        frame = FrameIChannel(tmp_name, channel, 1.0, gps)#, gps)

        # Store the data into the SV variable
        sv = SV()
        frame.GetData(sv)

        # Remove temporary file
        os.remove(tmp_name)

        return sv
