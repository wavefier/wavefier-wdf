from pytsa.tsa import SeqView_double_t as SV
# from pytsa.tsa import FrameIChannel
import logging
import os

from wavefier_wdf.util.Update import Update

from wavefier_common.RawData import RawData

from wdf.structures.array2SeqView import array2SeqView         # metti corretto import FORSE SU WDFPIPE 
from wdf.config.DataParameters import DataParameters
from wdf.config.WdfParameters import WdfParameters
from wdf.config.WhtParameters import WhtParameters
from wdf.config.DownsampParameters import DownsampParameters

class StaticSVCreator(object):
    """
    The following class recreates Sequence View file based on the binary data stored in kafka message

    Sequence View constructor requires:

    * aStart start of the sequence
    * aSampling sampling the sequence
    * ChannelSize the number of data in each channel
    * aName a name for the data
    * (optional) ColumnDim the dimension of the column structure

    """
    def __init__(self, wdf_parameters: WdfParameters, w_parameters: WhtParameters, downsamp_parameters: DownsampParameters, data_parameters: DataParameters, sv_limit: int=40): # was 40, I assume should be equal to learn
        """
        The following class recreates Sequence View file based on the binary data stored in kafka message

        Keyword arguments
        :type stream_data: RawData
        :param stream_data: stream of binary data from kafka message; contains gps time, data and filename
        """
        self.wdf_parameters = wdf_parameters
        self.w_parameters = w_parameters
        self.data_parameters = data_parameters
        self.downsamp_parameters = downsamp_parameters
        self.sampling = data_parameters.sampling
        self.length = data_parameters.len
        self.gps = data_parameters.gpsstart

        self.static_data = None
        self.sv_counter = 0
        self.sv_limit = sv_limit   

    def generate(self, stream_data: RawData):

        channel = os.environ["CHANNEL"]              # SHOULD BE PUT IN main

        if self.sv_counter == self.sv_limit:
            Update.update_for_wdf(self.wdf_parameters, self.w_parameters, self.downsamp_parameters, self.data_parameters, self.static_data, channel)
            return True
        else:
            self.initial_sv(stream_data, channel)
            return False


    def initial_sv(self, stream_data: RawData, channel: str):

        logging.info(self.sv_counter)

        # Save the data into temporary file
        #gps = stream_data.getStartGPSTime()
        gps = 0.0
        stream = stream_data.getData()
#        sv =       # inizializza una sequenza
        sv = array2SeqView(self.gps, self.sampling, self.length)
        sv = sv.Fill(self.gps, stream)

        single_data_size = self.length       ##### fissa come parametro di configurazione nel main.py

        if self.sv_counter == 0:
            total_data_size = self.sv_limit * single_data_size
            self.static_data = SV(sv.GetStart(), sv.GetSampling(), total_data_size, channel)

        min_range = self.sv_counter*single_data_size
        max_range = (self.sv_counter+1)*single_data_size
        sv_iterator = 0
        for i in range(min_range, max_range):
            self.static_data.FillPoint(0, i, sv.GetY(0, sv_iterator))
            self.static_data.FillPoint(0, i, sv.GetY(0, sv_iterator))
            sv_iterator += 1

        self.sv_counter += 1



    def update_sv(self, stream_data: RawData, channel: str):
        self.sv_counter = 0
        min_range = self.sv_counter*self.single_data_size
        max_range = (self.sv_counter+1)*self.single_data_size
        sv_iterator = 0

        if self.sv_counter == 0:
            total_data_size = self.sv_limit * self.single_data_size
            self.static_data = SV(sv.GetStart(), sv.GetSampling(), total_data_size, channel)

        for i in range(min_range, max_range):
            self.static_data.FillPoint(0, i, sv.GetY(0, sv_iterator))
            sv_iterator += 1

        self.sv_counter += 1