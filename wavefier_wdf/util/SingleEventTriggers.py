import os.path
import os
import logging
from wdf.observers.observer import Observer

from wavefier_wdf.util.DictToTrigger import DictToTrigger

from wavefier_trigger_handlers.TriggerDistributor import TriggerDistributor

from wdf.config.WdfParameters import WdfParameters              
from wdf.config.WhtParameters import WhtParameters
from wdf.config.DataParameters import DataParameters



class SingleEventTriggers(Observer):
    """
    This class stands for the conversion to dictionary generated single trigger
    """
    def __init__ ( self,  wdf_parameters: WdfParameters, w_parameters: WhtParameters):
        """
        This class stands for the conversion to dictionary generated single trigger

        :type par: WdfParam
        :param par: Set of WDF parameters

        :type fullPrint: int
        :param fullPrint: The number indicating type of returned triggers: 0 - only headers, 1 - header and wavelet coefficients, 2 - header, wavelet coefficients and raw wavelets; default value = 0
        """
        self.wdf_parameters = wdf_parameters
        self.w_parameters = w_parameters
        self.id = 0
        self.fullPrint = wdf_parameters.fullPrint
        self.headers = ['gps', 'snr', 'snrMax', 'freq', 'freqMax', 'duration', 'wave']
        self.headersFull = ['gps', 'snr', 'snrMax', 'freq', 'freqMax', 'duration', 'wave']
        for i in range(self.wdf_parameters.Ncoeff):
            self.headers.append("wt" + str(i))
            self.headersFull.append("wt" + str(i))
        for i in range(self.wdf_parameters.Ncoeff):
            self.headersFull.append("rw" + str(i))


    def update(self, CEV: object):
        """
        :type CEV: object
        :param CEV: The object storing single trigger data
        :return: single trigger in the form of dictionary
        """
        self.ev = CEV.__dict__
        self.id += 1
        self.ev['ID'] = self.id
        trigger_content = None
        if self.fullPrint == 1:
            trigger_content = dict((k, self.ev[k]) for k in self.headers)
        if self.fullPrint == 2:
            trigger_content = dict((k, self.ev[k]) for k in self.headersFull)
        if trigger_content:
            dt = DictToTrigger(trigger_content, self.ev["Ncoeff"], self.wdf_parameters.uuid, self.w_parameters.uuid)
            trigger = dt.convert()

            broker = os.environ['KAFKA_BROKER']
            registry = os.environ["KAFKA_REGISTRY"]

            broker_parts = broker.split(":")
            registry_parts = registry.split(":")

            broker_host = broker_parts[0]
            broker_port = broker_parts[1]

            registry_host = registry[:-len(registry_parts[-1])-1]
            registry_port = registry_parts[-1]
            dist = TriggerDistributor(broker_host, broker_port, registry_host, registry_port)
            result = dist.deploy(os.environ["CHANNEL"], self.w_parameters, self.wdf_parameters, trigger) # cross check "CHANNEL"?
            print("Deployed: ")
            print(result)
