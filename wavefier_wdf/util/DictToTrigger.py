from wavefier_common.Trigger import Trigger

import os

class DictToTrigger(object):
    """
    This class is responsible for conversion of dictionary (output of WDF) into Trigger object
    """

    def __init__(self, trigger_dict: dict, Ncoeff: int, wdf_uuid: str, wth_uuid: str):
        """
        DictToTrigger

        Keyword arguments:
            trigger_dict {dict} - Output of WDF
            Ncoeff {int} - number of wavelet coefficients from WDF
        """
        self.trigger_dict = trigger_dict
        self.Ncoeff = Ncoeff
        self.wdf_uuid = wdf_uuid
        self.wth_uuid = wth_uuid

    def convert(self) -> Trigger:
        """
        Convert the dictrionary output of WDF into Trigger object
        :return: Trigger object
        """
        wt = {}
        rw = {}
        for i in range(self.Ncoeff):
            wt["wt" + str(i)] = float(self.trigger_dict["wt" + str(i)])

        for i in range(self.Ncoeff):
            rw["rw" + str(i)] = float(self.trigger_dict["rw" + str(i)])

        trigger = Trigger(
            WDFParam_id=self.wdf_uuid,
            WTHParam_id=self.wth_uuid,
            gps=self.trigger_dict["gps"],
            snr=self.trigger_dict["snr"],
            snrMax=self.trigger_dict["snrMax"],
            freq=self.trigger_dict["freq"],
            freqMax=self.trigger_dict["freqMax"],
            duration=self.trigger_dict["duration"],
            wave=self.trigger_dict["wave"],
            nCoeff=self.Ncoeff,
            channel=os.environ["CHANNEL"],
            partition=1,
            wt=wt,
            rw=rw
        )

        return trigger
