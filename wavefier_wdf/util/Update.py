from pytsa.tsa import SeqView_double_t
import logging

#from wavefier_common.param.WitheningParam import WitheningParam      #outdated
#from wavefier_common.param.WdfParam import WdfParam

from wdf.config.WdfParameters import WdfParameters
from wdf.config.WhtParameters import WhtParameters
from wdf.config.DataParameters import DataParameters
from wdf.config.DownsampParameters import DownsampParameters

class Update:

    """
    This class is reponsible for the update of WDF parameters
    """

    @staticmethod
    def update_for_wdf(wdf_parameters: WdfParameters, w_parameters: WhtParameters, downsamp_parameters: DownsampParameters, data_parameters: DataParameters, sv: SeqView_double_t, channel: str):
        """
        This methods updates WDF parameters for the given SeqView data

        :type wdf_parameters: WdfParam
        :param wdf_parameters: Set of WDF parameters

        :type w_parameters: WitheningParam
        :param w_parameters: Set of whitening parameters

        :type sv: pytsa.tsa.SeqView_double_t
        :param sv: Sequence View object

        :type channel: str
        :param channel: Name of the analysed channel from the incoming data
        """

        logging.info("WDF parameters are being updated")
        wdf_parameters.__setattr__("sampling", int(1.0 / sv.GetSampling()))
        wdf_parameters.__setattr__("resampling", int(data_parameters.sampling / downsamp_parameters.ResamplingFactor))
        wdf_parameters.__setattr__("gps", sv.GetStart())
        wdf_parameters.__setattr__("gpsStart", sv.GetStart())
        wdf_parameters.__setattr__("gpsEnd", sv.GetEnd())
        wdf_parameters.__setattr__("channel", data_parameters.channel)
        wdf_parameters.__setattr__("ARorder", w_parameters.ARorder)
        wdf_parameters.__setattr__("Noutdata", 1.0 * float(wdf_parameters.resampling))
        logging.info("Update finished")
        logging.info(wdf_parameters.__dict__)
