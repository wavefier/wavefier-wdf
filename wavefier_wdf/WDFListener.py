import logging
import os

from wavefier_import_handlers.RawListener import RawListener
from wavefier_import_handlers.RawSupplier import RawSupplier       # unused??

from wavefier_common.RawData import RawData

from wavefier_wdf.util.SVCreator import SVCreator
from wavefier_wdf.util.StaticSVCreator import StaticSVCreator           # unused
from wavefier_wdf.preprocessing.ApplyStaticWhitening import ApplyStaticWhitening # unused,import wdf.processes.whitening?
from wdf.processes.whitening import Whitening

from wdf.config.DataParameters import DataParameters
from wdf.config.WdfParameters import WdfParameters
from wdf.config.WhtParameters import WhtParameters
from wdf.processes.wdf import wdf
from util.SingleEventTriggers import SingleEventTriggers
from util.ParameterEstimation import ParameterEstimation

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class WDFListener(RawListener):

    def __init__(self,  whiten: Whitening, wdf_params: WdfParameters, wht_params: WhtParameters, data_params: DataParameters):
        # Initialize parameters
        self.whiten = whiten
        self.channel = data_params.channel

        self.wdf_parameters = wdf_params
        self.wht_parameters = wht_params
  #      self.fullPrint= wdf_params.fullPrint      

        self.svc = SVCreator()


        # Initialize WDF
        self.WDF = wdf(self.wdf_parameters)

        ## register obesevers to WDF process
        logging.info("Register observers to WDF process")
        savetrigger = SingleEventTriggers(self.wdf_parameters, self.wht_parameters)

        # Parameter Estimation class requires only two parameters: resampling and Ncoeff
        parameterestimation = ParameterEstimation(self.wdf_parameters, data_params)
        parameterestimation.register(savetrigger)
        

        self.WDF.register(parameterestimation)

    def newRawdataAppears(self, raw_data: RawData):
        """
        Executed whenever a rawdata is present
        :type rawdata: RawData
        :param rawdata: Data to be processed
        :return:
        """
        # Create single SV
        sv = self.svc.generate(raw_data, self.channel)

        svw = self.svc.generate(raw_data, self.channel)

        # Whiten the SV
        self.whiten.Process(sv, svw)

        # Run WDF
        logging.info("Running WDF")
        self.WDF.SetData(svw)
        self.WDF.Process()

    def update(self, context, msg_consumed):
        if msg_consumed is not None:
            if msg_consumed.getData() is not None:
                if self.whiten.GetSigma() :
                    self.newRawdataAppears(msg_consumed)
                else:
                    logging.info("Skip this data because Whitening Coeficient is not ready!")
