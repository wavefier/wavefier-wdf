from wavefier_import_handlers.RawSupplier import RawSupplier
from wdf.config.WdfParameters import WdfParameters
from wdf.config.WhtParameters import WhtParameters
from wdf.config.DownsampParameters import DownsampParameters
from wdf.config.DataParameters import DataParameters

from wdf.processes.whitening import Whitening
from WDFListener import WDFListener
from WhiteningCoefficientsDiscoverer import WhiteningCoefficientsDiscoverer

# from wdf.config.withening.WitheningCoef import WitheningCoef      # ALB: removed, does not exist anymore
import logging



import os

if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)
    logging.info("read parameters from JSON file")

    KAFKA_BROKER = os.environ['KAFKA_BROKER']
    CHANNEL = os.environ['CHANNEL']

    # par = Parameters()
    # filejson = "nested_config.json"
    # try:
    #     par.load(filejson)
    # except IOError:
    #     logging.error("Cannot find resource file " + filejson)
    #     quit()

    wdf_params = WdfParameters()
    wht_params = WhtParameters()
    downsamp_params = DownsampParameters()
    data_params = DataParameters(CHANNEL)

    filejson_wdf = "wdf_split_config.json"                   # we can set as a loop, tries to load all the config files
    filejson_wht = "wht_split_config.json"
    filejson_downsamp = 'downsamp_split_config.json'        # add if necessary
    filejson_data = "data_split_config.json"
    try:
        wdf_params.load(filejson_wdf)
    except IOError:
        logging.error("Cannot find resource file " + filejson_wdf)
    try:
        wht_params.load(filejson_wht)
    except IOError:
        logging.error("Cannot find resource file " + filejson_wht)
    try:
        data_params.load(filejson_downsamp)
    except IOError:
        logging.error("Cannot find resource file " + filejson_downsamp)
    try:
        data_params.load(filejson_data)
    except IOError:
        logging.error("Cannot find resource file " + filejson_data)

    #ADD ESTRA CONFIGURATION
    data_params.broker = KAFKA_BROKER
        
    logging.info("channel= %s at sampling frequency= %s" %(data_params.channel, data_params.sampling))

    ####### Istantiate whitening with selected ARorder, search for coefficient files if exist #######
    whiten= Whitening(wht_params.ARorder)                

    iWdf = WDFListener(whiten, wdf_params, wht_params, data_params)
    iWhiteningCoefficientsDiscoverer = WhiteningCoefficientsDiscoverer(whiten, wht_params.learn_step, wdf_params, wht_params, downsamp_params, data_params)


    sup = RawSupplier(data_params.broker)
    sup.addRawListener(iWhiteningCoefficientsDiscoverer)
    sup.addRawListener(iWdf)
    sup.startObserving()
