import logging
import os

import datetime as dt

from wavefier_import_handlers.RawListener import RawListener
from wavefier_import_handlers.RawSupplier import RawSupplier

from wavefier_common.RawData import RawData

from wavefier_wdf.util.SVCreator import SVCreator
from wavefier_wdf.util.StaticSVCreator import StaticSVCreator
from wavefier_wdf.ApplyWDF import ApplyWDF
from wavefier_wdf.preprocessing.ApplyStaticWhitening import ApplyStaticWhitening
#   from wdf.config.withening.WitheningDataCoef import WitheningDataCoef               # ALB: removed, not present in wdf master branch  anymore

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class WhiteningCoefficientsDiscoverer(RawListener):

    def __init__(self,whiten: Whitening, learnStepInSec: int):
        # Initialize parameters
        self.static_sv = StaticSVCreator(wdf_parameters, wht_params)
        self.lastEstimation = 0 
        self.learnStepInSec=learnStepInSec
        self.whiten= whiten
        
    def estimateWhitening(self, raw_data: RawData):
        logging.info("Static Whitening preparation")
        static_flag = self.static_sv.generate(raw_data)

        if static_flag:
            # Run Adaptive whitening
            self.whiten.ParametersEstimate(self.static_sv.static_data)
            self.lastEstimation = 0    # everytime whitening coefficients are estimated, restart counter 
        else:
            logging.warning("Static Flag False")


    def update(self, context, msg_consumed):
        if msg_consumed is not None:
            if msg_consumed.getData() is not None:
                ## TODO: REVIEW SUL QUANDO SCATTA IL LEARNING
                if ((not self.whiten.GetSigma()) and (self.lastEstimation > self.learnStepInSec)):
                    self.estimateWhitening(msg_consumed)
                else:
                    self.lastEstimation += 1  # add to counter every message consumed ----> NOT SURE ABOUT THIS!
                    logging.info("Learning is not required...")


