FROM wdfteam/wdfpipe:2.1.0latest

RUN mkdir /wavefier-wdf

WORKDIR /wavefier-wdf

COPY .  /wavefier-wdf

RUN apt-get --force-yes update
#RUN apt-get --assume-yes install librdkafka1
RUN apt-get --assume-yes install librdkafka-dev

RUN python wavefier_wdf/igwn-lldd-main/setup.py install

RUN python setup.py install

RUN pip uninstall -y wavefier-common

RUN python setup.py develop

CMD ["python", "-u", "wavefier_wdf/WatcherRawData.py"]


