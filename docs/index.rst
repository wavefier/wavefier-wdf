.. wavefier-wdf documentation master file, created by
   sphinx-quickstart on Wed Jan 23 11:56:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wavefier-wdf's documentation!
========================================

The wavefier-wdf library is part of the Wavefier project. It is responsible for the data processing from Kafka cluster
and the application of the Wavelet-Denoising-Filter (WDF) on that data. The core part of the wavefier-wdf is based on the
WDF implementation p4TSA developed by Elena Cuoco(see`p4TSA github repository <https://github.com/elenacuoco/p4TSA/>`_).
p4TSA can be wrapped in Python and within this library it is denoted by `pytsa`.


Table of content
=================

.. toctree::
   :maxdepth: 2

   structure/introduction
   structure/tutorial
   structure/code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
