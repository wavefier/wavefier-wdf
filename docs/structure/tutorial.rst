Tutorial
=====================

From the main directory of the library, open `tests` folder. There you will find few examples of how the code works.
Currently there are 3 scripts:

* test_stream.py - tests the whole library by running RunWDF.py script on the sample data from `test_data` folder

* test_SV_generation.py - tests generation of the Sequence View data from the stream of binary data

* test_whitening.py - tests the whitening procedure on the sample data

To run the script, write in the termina, e.g.:

``python test_stream.py``

On the screen, you will set of logs and informations regarding various steps of processing the data.