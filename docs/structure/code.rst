The library's code
=======================

The library has been designed that various parts of transient signal detection are separated from each other. The most
fundamental functions are stored in the **core** directory. They are responsible for the communication with p4TSA
library in order to process data in particular way like: downsampling, whitening or applying WDF.
Directories **wdf** and **preprocessing** are responsible for the communication between the **core** and the main
scripts of wavefier-wdf serving as APIs:

* `WDFListener`


Library's content
-------------------

.. toctree::
   :maxdepth: 5

   libs/apis.rst
   libs/util.rst
   libs/wdf.rst
   libs/preprocessing.rst
   libs/core/observers.rst
   libs/core/structures.rst
   libs/core/processes.rst
