Introduction
=============

WDF is Wavelet denoising sth

Requirements
-------------

- wavefier-common
- wavefier-import-handler
- wavefier-trigger-handlers
- p4TSA
- librosa
- scipy (upgraded version)
- docker with WDF environment

Installation
-------------

To install the wavefier-wdf library, one has to run `setup.py` script from the main directory of the library.

``python setup.py install``
