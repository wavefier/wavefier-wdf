The processes functions
============================


filtering
-------------

.. automodule:: core.processes.filtering
   :members:

wdf
-------------

.. automodule:: core.processes.wdf
   :members:

whitening
-------------

.. automodule:: core.processes.whitening
   :members:

