The observers functions
============================


observer
-------------

.. automodule:: core.observers.observer
   :members:

observable
-------------

.. automodule:: core.observers.observable
   :members:

ParameterEstimationObserver
----------------------------

.. automodule:: core.observers.ParameterEstimationObserver
   :members:

SingleEventPrintFileObserver
-----------------------------

.. automodule:: core.observers.SingleEventObserver
   :members:
