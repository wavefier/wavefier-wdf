The preprocessing functions
============================


Downsampling
-------------

.. automodule:: preprocessing.ApplyDownsampling
   :members:

Whitening
-----------

.. automodule:: preprocessing.ApplyWhitening
   :members:

Static Whitening
------------------

.. automodule:: preprocessing.ApplyStaticWhitening
   :members:

