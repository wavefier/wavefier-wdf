The util functions
---------------------

Update
-----------


.. automodule:: util.Update
   :members:


DictToTrigger
------------------

.. automodule:: util.DictToTrigger
   :members:

SVCreator
----------------

.. automodule:: util.SVCreator
   :members:

StaticSVCreator
-----------------

.. automodule:: util.StaticSVCreator
   :memebers:

