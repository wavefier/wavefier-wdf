# Wavefier WDF Library
This library is responsible for the application of Wavelet Denoising Filter (WDF) on the data incoming from Kafka Cluster.

### Create Source dist build
To create a source distribution, you run: `python setup.py sdist`. It create a tar gz on a directory dist with a source distribution

### Create Binary dist build
To create a binary distribution called a wheel, you run: `python setup.py bdist`

Other opions can be used:
* `python setup.py bdist --formats=rpm` rpm Format
* `python setup.py bdist --formats=wininst` window installer 

## Development 
Current version - 0.0.1

Required libraries:
* wavefier-common - `rawdata` branch
* wavefier-sv
* wavefier-trigger-handlers
* librosa
* upgraded version of scipy (run `pip install --upgrade scipy`)

### Run test
To run the test, enter the 'tests' directory and run one of existing scripts, like: `python test_stream.py` 

### Run Documentation
To create documentation use the `make clean && make docs`. On the directory docs you can see the generated documentation
To run the website go to the _build/html subdirectory. There open `index.html` website.

THe documentation tree is stored in the `index.rst` file and the `structure` directory
