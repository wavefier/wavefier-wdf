from wavefier_wdf.WDFListener import WDFListener
import os

class Stream:
    def __init__(self, name_file):
        self.name_file = name_file

    def getData(self):
        f = open(self.name_file, "rb")
        return f.read()

    def getStartGPSTime(self):
        splitted_name = self.name_file.split("-")
        return float(splitted_name[-1][:-4])



if __name__ == "__main__":

    wdf = WDFListener()

    files_list = os.listdir("./test_data/")

    for item in files_list:
        raw_data = Stream("./test_data/" + item)

        wdf.update(None, raw_data)
