#!/bin/bash

TOPIC_NAME=$2
KAFKA_HOST=$1
KAFKA_WP=/opt/kafka_2.11-0.10.2.2

CMD="kafka-console-producer.sh --broker-list ${KAFKA_HOST}:9092 --topic ${TOPIC_NAME}"

${KAFKA_WP}/bin/${CMD}
